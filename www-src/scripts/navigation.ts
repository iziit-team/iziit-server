enum State {
	Closing = 'closing',
	Hidden = 'hidden',
	Opening = 'opening',
	Visible = 'visible',
}

class NavButton {

	private classList: DOMTokenList;
	private animationLength: number;
	private animationId: number;
	private animationStart = NaN;
	private state: State;

	constructor(element: HTMLElement, animationLength: number) {
		this.classList = element.classList;
		this.animationLength = animationLength;
		this.setState(State.Hidden);
		element.addEventListener('click', () => this.onClick());
	}

	private setState(state: State) {
		if (this.state === state) return;
		if (this.state) {
			this.classList.remove(this.state);
			if (this.state === State.Hidden) {
				document.body.classList.add('prevent-scrolling');
			} else if (state === State.Hidden) {
				document.body.classList.remove('prevent-scrolling');
			}
		}
		this.state = state;
		this.classList.add(state);
	}

	private setFutureState(state: State) {
		let delay: number;
		if (this.animationId) {
			clearTimeout(this.animationId);
			delay = this.animationStart + this.animationLength - Date.now();
		} else {
			delay = this.animationLength;
			this.animationStart = Date.now();
		}
		this.animationId = setTimeout(() => {
			this.setState(state);
			this.animationId = undefined;
		}, delay) as unknown as number; // because we use the browser implementation of setTimeout, not the NodeJS one
	}

	private onClick() {
		switch (this.state) {
			case State.Closing:
			case State.Hidden:
				this.setState(State.Opening);
				this.setFutureState(State.Visible);
				break;
			case State.Opening:
			case State.Visible:
				this.setState(State.Closing);
				this.setFutureState(State.Hidden);
				break;
		}
	}
}

// tslint:disable-next-line: no-unused-expression
new NavButton(document.getElementById('nav-logo'), 500);
