import bootbox from './bootbox-loader';

let captchaResponse;
let captchaLoaded = false;
const textArea = document.querySelector('textarea');
const emailField = document.querySelector('input[name="email"]');

window.captchaValidatedCallback = (response) => {
	captchaResponse = response;
};

window.captchaExpiredCallback = () => {
	captchaResponse = undefined;
};

function loadCaptcha() {
	const captchaScript = document.createElement('script');
	captchaScript.setAttribute('src', 'https://www.google.com/recaptcha/api.js');
	document.head.appendChild(captchaScript);
	captchaLoaded = true;
}

if (textArea.value === ''){
	const consentOverlay = document.getElementById('consent-overlay');
	const continueButton = consentOverlay.querySelector('input[name="continue"]');
	const cancelButton = consentOverlay.querySelector('input[name="cancel"]');

	textArea.addEventListener('click', () => {
		if (!captchaLoaded) {
			textArea.blur();
			consentOverlay.classList.add('activated');
		}
	});

	continueButton.addEventListener('click', () => {
		consentOverlay.classList.remove('activated')
		textArea.focus();
		loadCaptcha();
	});

	cancelButton.addEventListener('click', () => {
		consentOverlay.classList.remove('activated');
	});
} else {
	loadCaptcha();
}

function sendForm() {
	const xhr = new XMLHttpRequest();
	xhr.open('POST', '/contact/send-form');
	xhr.setRequestHeader('Content-Type', 'application/json');

	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				emailField.value = '';
				textArea.value = '';
				bootbox.alert('Votre message a bien été reçu.');
			} else {
				bootbox.alert(`Une erreur est survenue (${xhr.status} ${xhr.statusText}).<br/>Réessayez plus tard ou contactez-nous par mail.`);
			}
		}
	};

	xhr.send(JSON.stringify({
		email: emailField.value,
		message: textArea.value,
		captchaResponse: captchaResponse,
	}));

	grecaptcha.reset();
	captchaResponse = undefined;
}

document.querySelector('input[name="send"]').addEventListener('click', () => {

	if (textArea.value === '') {

		textArea.className = 'flickering';
		setTimeout(() => textArea.className = '', 750);

	} else if (captchaResponse === undefined) {

		bootbox.alert('Vous n\'avez pas validé le captcha.');

	} else if (emailField.value === '') {
		bootbox.confirm({
			message: 'Vous n\'avez pas indiqué votre addresse mail donc nous ne pourrons pas vous répondre.',
			buttons: {
				confirm: {
					label: 'renseigner l\'adresse',
					className: 'btn-success'
				},
				cancel: {
					label: 'continuer',
					className: 'btn-danger'
				}
			},
			callback: (confirm) => {
				if (confirm) {
					emailField.className = 'flickering';
					setTimeout(() => emailField.className = '', 1250);
				} else {
					sendForm();
				}
			}
		});
	} else {
		sendForm();
	}
});
