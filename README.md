# Iziit Server

licence: [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.txt)  
website: [iziit.org](https://iziit.org)  
contact: [contact@iziit.org](mailto:contact@iziit.org)

## Development environment

### Prerequisites

Make sure that [Node.js](https://nodejs.org) and [MongoDB](https://www.mongodb.com/) are installed.

### Setting it up

1. Clone the repository:  
using SSH: `git clone git@gitlab.com:iziit-team/iziit-server.git`  
using HTTPS: `git clone https://gitlab.com/iziit-team/iziit-server.git`

2. Install dependencies:  
`$ npm install`

3. Generate a private key and a certificate in order to use HTTPS. This command generates them for the domain name "localhost":  
```bash
$ openssl req -x509 -out localhost.crt -keyout localhost.key \
    -newkey rsa:2048 -nodes -sha256 \
    -subj '/CN=localhost' -extensions EXT -config <( \
     printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

4. Create the configuration file (see bellow).

### Configuration file

The configuration file must be named `config.json` and must be at the project root. It have the following architecture:

```json
{
	"http": {
		"port": number
	},
	"https": {
		"port": number,
		"secretKeyLocation": string,
		"certificateLocation": string,
	},
	"reCaptchaSecretKey": string,
	"addonBackend": {
		"maxProductsPerRequest": number,
		"databases": {
			"host": string,
			"port": number,
			"updateTime": string,
			"nbOfDaysBeforeDeletionWhenUnused": number,
			"nbOfDaysBeforeUpdating": number,
			"credentials": {
				"login": string,
				"password": string,
				"authSource": string
			},
			"options": object
		}
	}
}
```

#### Optionnal fields

* `http.port` is optionnal, default is `80`
* `https` can be omited if you want only HTTP
* `https.port` is optionnal, default is `443`
* `reCaptchaSecretKey` can be omited while the captcha isn't used.
* `addonBackend` can be omited if the `--front-only` option is used.
* `addonBackend.databases.host` is optionnal, default is `localhost`
* `addonBackend.databases.port` is optionnal, default is `27017`
* `addonBackend.databases.credentials` can be omited if the MongoDB server doesn't require authentication.

#### Explanations

**http.port**
The port of the HTTP server (default is `80`).
The HTTP server will redirect to the HTTPS server if it's running.

**https.port**
The port of the HTTPS server (default is `443`).

**https.secretKeyLocation:**  
The absolute path to the secret key generated at step 3.

**https.certificateLocation:**  
The absolute path to the certificate generated at step 3.

**reCaptchaSecretKey:**   
The secret key for the Google's reCaptcha API.

**addonBackend.maxProductsPerRequest:**  
The maximum number of products that a client request can contains. Must be 100 unless this value was changed on the client side too.

**addonBackend.databases.updateTime:**  
The time at witch the databases will start updating their data. It must be in the `hh:mm` 24 hours clock format. For example, this field can be `"02:00"` or `"23:59"`.

**addonBackend.databases.nbOfDaysBeforeDeletionWhenUnused:**  
The number of days past which, the composition of a product will be deleted from the databases if it wasn't consulted.

**addonBackend.databases.nbOfDaysBeforeUpdating:**  
The number of days past which, the composition of a product will be updated to match any potential change from the website composition.

**addonBackend.databases.credentials.login:**  
The MongoDB server login.

**addonBackend.databases.credentials.password:**  
The MongoDB server password.

**addonBackend.databases.credentials.authSource:**  
The MongoDB database name where the user credentials are stored. By default the MongoDB server stores them in the `admin` database.

**addonBackend.databases.options:** 
The MongoDB database options.

### Usage

#### To work on frontend

Start the server as a static server only:  
`npm run static-server`

Then, you can watch for changes in the `www-src` directory by doing:  
`npm run dev-front`

#### To work on backend

You must start a MongoDB server first. Then you can start Iziit Server:  
`npm run server`

#### To work on both

You must start a MongoDB server first. Then you can start Iziit Server:  
`npm server.js`

Finally, you can watch for changes in the `www-src` directory by doing:  
`npm run dev-front`

#### To build the ready for production static resources

Simply run:  
`npm run build-front`
