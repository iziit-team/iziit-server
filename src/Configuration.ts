// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { readFileSync } from 'fs';
import { resolve } from 'path';
import Skeletons from 'skeletons';
import { ServerConfig } from './Types';
import { isFileReadable } from './Utils';

const credentialsStructure = Skeletons.Object({
	object: {
		authSource: Skeletons.String({
			minLength: 1,
		}),
		login: Skeletons.String({
			minLength: 1,
		}),
		password: Skeletons.String({
			minLength: 1,
		}),
	},
	required: false,
});

const databaseConfigStructure = Skeletons.Object({
	object: {
		credentials: credentialsStructure,
		host: Skeletons.String({
			required: false,
		}),
		nbOfDaysBeforeDeletionWhenUnused: Skeletons.Number({
			allowNaN: false,
			min: 0,
		}),
		nbOfDaysBeforeUpdating: Skeletons.Number({
			allowNaN: false,
			min: 0,
		}),
		options: Skeletons.Object({
			extraKey: true,
			required: false,
		}),
		port : Skeletons.Number({
			allowNaN: false,
			min: 1,
			required: false,
		}),
		updateTime: Skeletons.String({
			match: /^(?:(?:[01]\d)|(?:2[0123])):[012345]\d$/,
		}),
	},
	required: false,
});

const addonBackendConfigStructure = Skeletons.Object({
	object: {
		databases: databaseConfigStructure,
		maxProductsPerRequest: Skeletons.Number({
			allowNaN: false,
			min: 1,
		}),
	},
	required: false,
});

const httpConfigStructure = Skeletons.Object({
	object: {
		port: Skeletons.Number({
			allowNaN: false,
			min: 0,
			required: false,
		}),
	},
	required: false,
});

const httpsConfigStructure = Skeletons.Object({
	object: {
		certificateLocation: Skeletons.String({
			validator: isFileReadable,
		}),
		port: Skeletons.Number({
			allowNaN: false,
			min: 0,
			required: false,
		}),
		secretKeyLocation: Skeletons.String({
			validator: isFileReadable,
		}),
	},
	required: false,
});

const serverConfigStructure = Skeletons.Object({
	object: {
		addonBackend: addonBackendConfigStructure,
		http: httpConfigStructure,
		https: httpsConfigStructure,
		reCaptchaSecretKey: Skeletons.String({
			match: /^[0-9a-zA-Z_-]{40}$/,
			required: false,
		}),
	},
});

const serverConfigValidator = new Skeletons(serverConfigStructure);

function isServerConfigValid(config: any): config is ServerConfig {
	return serverConfigValidator.validate(config).valid;
}

function loadServerConfig(): ServerConfig {
	const serverConfigFromFile = JSON.parse(readFileSync(resolve(__dirname, '..', 'config.json'), 'utf8'));
	return isServerConfigValid(serverConfigFromFile) ? serverConfigFromFile : undefined;
}

export const serverConfig = loadServerConfig();
export const addonBackendConfig = serverConfig?.addonBackend;
export const databaseConfig = addonBackendConfig?.databases;
