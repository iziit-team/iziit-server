// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import autostrada from 'autostrada';
import { json } from 'body-parser';
import * as compression from 'compression';
import * as express from 'express';
import { resolve } from 'path';
import AddonBackend from './AddonBackend';
import contactFormHandler from './ContactFormHandler';
import Server from './Server';

const frontOnly = ~process.argv.indexOf('--front-only') ? true : false;
const app = express();

function setupRoutes() {
	// compress response
	app.use(compression());

	// auto route www directory
	app.use(autostrada({
		fromPathToUrl: (path: string) => {
			// '/xxx/yyy/index.html' => ['/xxx/yyy/', '/xxx/yyy']
			if (path.match(/^((.*)\/)index\.html$/)) return [RegExp.$1, RegExp.$2];
			// '/xxx/yyy/zzz.html' => '/xxx/yyy/zzz'
			if (path.match(/^(.*)\.html$/)) return RegExp.$1;
			// no mofification
			return path;
		},
		wwwDirectory: resolve(__dirname, '..', 'www'),
	}));

	if (!frontOnly) {
		// body parser (parses incoming request bodies and stores the result in req.body)
		app.use(json()); // parses json only

		// addon backend
		app.use(AddonBackend.middleware);

		// contact/send-form
		app.post('/contact/send-form', contactFormHandler);

		// 404 page
		app.use ('/', (_req, res) => {
			res.status(404).send('404 Not Found');
		});
	}
}

async function initAddonBackend() {
	if (frontOnly) return;
	try {
		await AddonBackend.init();
	} catch (err) {
		console.error(err);
	}
}

async function main() {
	setupRoutes();
	await initAddonBackend();
	Server.runApp(app);
}

main();
