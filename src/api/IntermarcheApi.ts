// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Response } from 'express';
import Skeletons from 'skeletons';
import { addonBackendConfig } from '../Configuration';
import IntermarcheDbHandler from '../database/db-handlers/IntermarcheDbHandler';
import { isIngredientList } from '../SupervisedIngredients';
import { Brand, PayloadBase } from '../Types';
import Api from './Api';

interface IntermarchePayload extends PayloadBase {
	products: number[];
	protocolVersion: number;
	shop: string;
}

interface ResponsePayload {
	[key: number]: boolean;
}

export default class IntermarcheApi extends Api<IntermarcheDbHandler, IntermarchePayload> {

	public constructor() {
		super(Brand.Intermarche);
	}

	protected async handleClientPayload(payload: IntermarchePayload, res: Response) {
		const promises = [];
		const responsePayload: ResponsePayload = {};
		for (const product of payload.products) {
			promises.push(
				this.dbHandler.getProductComposition(payload.shop, product)
				.then((composition) => {
					if (!composition) return;
					responsePayload[product] = !Api.doesProductContainsIngredientToAvoid(payload.ingredientsToAvoid, composition);
				}).catch(console.error));
		}
		await Promise.all(promises);
		res.setHeader ('Content-Type', 'application/json');
		res.status(200).send(JSON.stringify(responsePayload));
	}

	protected getPayloadValidator(): Skeletons {
		return new Skeletons({
			ingredientsToAvoid: Skeletons.Array({
				validator: isIngredientList,
			}),
			products: Skeletons.Array({
				item: Skeletons.Number({
					min: 0,
				}),
				maxLength: addonBackendConfig.maxProductsPerRequest,
				minLength: 1,
			}),
			protocolVersion: Skeletons.Number({
				strictEquals: 1,
			}),
			shop: Skeletons.String({
				minLength: 1,
			}),
		});
	}
}
