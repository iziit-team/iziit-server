// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Response } from 'express';
import Skeletons from 'skeletons';
import { addonBackendConfig } from '../Configuration';
import LeclercDbHandler from '../database/db-handlers/LeclercDbHandler';
import { isIngredientList } from '../SupervisedIngredients';
import { Brand, PayloadBase } from '../Types';
import Api from './Api';

interface Product {
	id: number;
	productPage?: string;
	name?: string;
}

interface LeclercPayload extends PayloadBase {
	protocolVersion: number;
	shopId: number;
	products: Product[];
}

interface ResponsePayload {
	moreInfoNeeded: number[];
	products: {
		[key: number]: boolean,
	};
}

export default class LeclercApi extends Api<LeclercDbHandler, LeclercPayload> {

	public constructor() {
		super(Brand.Leclerc);
	}

	protected async handleClientPayload(payload: LeclercPayload, res: Response) {
		const promises = [];
		const responsePayload: ResponsePayload = { moreInfoNeeded: [], products: {} };
		for (const product of payload.products) {
			if (product.productPage) {
				promises.push(
					this.dbHandler.getProductComposition(product.productPage)
					.then((composition) => {
						if (!composition) return;
						responsePayload.products[product.id] = !Api.doesProductContainsIngredientToAvoid(payload.ingredientsToAvoid, composition);
					}).catch(console.error));
			} else {
				responsePayload.moreInfoNeeded.push(product.id);
			}
		}
		await Promise.all(promises);
		res.setHeader ('Content-Type', 'application/json');
		res.status(200).send(JSON.stringify(responsePayload));
	}

	protected getPayloadValidator(): Skeletons {
		return new Skeletons({
			ingredientsToAvoid: Skeletons.Array({
				validator: isIngredientList,
			}),
			products: Skeletons.Array({
				item: Skeletons.Object({
					object: {
						id: Skeletons.Number({
							min: 0,
						}),
						name: Skeletons.String({
							minLength: 1,
							required: false,
						}),
						productPage: Skeletons.String({
							minLength: 1,
							required: false,
						}),
					},
					validator: (value: Product) => {
						if (value.name && value.productPage) {
							return value.productPage.match(new RegExp(
								`^https:\\/\\/fd\\d+-courses\\.leclercdrive\\.fr\\/magasin-\\d+-[^?/]+\\/fiche-produits-${value.id}-[^?/]+\\.aspx$`))
								? true : false;
						} else {
							return !value.name && !value.productPage;
						}
					},
				}),
				maxLength: addonBackendConfig.maxProductsPerRequest,
				minLength: 1,
			}),
			protocolVersion: Skeletons.Number({
				strictEquals: 1,
			}),
			shopId: Skeletons.Number({
				min: 0,
			}),
		});
	}
}
