// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Request, Response } from 'express';
import Skeletons from 'skeletons';
import AddonBackend from '../AddonBackend';
import DbHandler from '../database/db-handlers/DbHandler';
import { Brand, Composition, IngredientsList, PayloadBase } from '../Types';

export default abstract class Api<H extends DbHandler, P extends PayloadBase> {

	protected static doesProductContainsIngredientToAvoid(ingredientsToAvoid: IngredientsList, productComposition: Composition): boolean {
		for (const ingredientToAvoid of ingredientsToAvoid) {
			if (productComposition[ingredientToAvoid]) return true;
		}
		return false;
	}

	protected dbHandler: H;
	private payloadValidator = this.getPayloadValidator();

	public constructor(brand: Brand) {
		this.dbHandler = AddonBackend.dbHandlersCoordinator.getDbHandler(brand) as H;
	}

	public handleClientRequest(req: Request, res: Response) {
		if (this.payloadValidator.validate(req.body).valid) {
			this.handleClientPayload(req.body as P, res);
		} else {
			res.setHeader('Content-Type', 'text/plain; charset=utf-8');
			res.status(400).send();
		}
	}

	protected abstract getPayloadValidator(): Skeletons;
	protected abstract async handleClientPayload(payload: P, res: Response): Promise<void>;
}
