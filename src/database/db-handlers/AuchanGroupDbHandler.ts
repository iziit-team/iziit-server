// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { JSDOM } from 'jsdom';
import * as request from 'superagent';
import { Response } from 'superagent';
import { Composition, Supplier } from '../../Types';
import { sendRequest } from '../../Utils';
import { MongodbProductDocument } from '../Documents';
import DbHandler from './DbHandler';

interface AuchanGroupProductPageUrlElements {
	id: string;
	name: string;
}

export default abstract class AuchanGroupDbHandler extends DbHandler {

	public async init(): Promise<void> {
		return Promise.resolve();
	}

	public getProductComposition(shopId: number, productPageUrl: string): Promise<Composition> {
		const productInfos = this.parseProductPageUrl(productPageUrl);
		return super.getProductComposition2(shopId, productInfos.id, productInfos.name, () => this.getProductCompositionOnline(shopId, productPageUrl));
	}

	protected async buildOnlineProductCompositionSupplier(shopId: number, document: MongodbProductDocument): Promise<Supplier<Promise<Composition>>> {
		return () => this.getProductCompositionOnline(shopId, this.buildProductPageUrl(document._id, document.name));
	}

	protected abstract buildProductPageUrl(id: number, name: string): string;
	protected abstract getProductCompositionListFromHtmlDocument(document: Document): string;
	protected abstract buildCookie(shopId: number): string;
	protected abstract getProductPageUrlParser(): RegExp;

	private async getProductCompositionOnline(shopId: number, productPageUrl: string): Promise<Composition> {

		let res: Response;

		try {
			res = await sendRequest(request.get(productPageUrl).set('Cookie', this.buildCookie(shopId)));
		} catch (err) {
			console.warn(`${err.status}: ${productPageUrl}, shopId=${shopId}`);
			return undefined;
		}

		const document: Document = new JSDOM(res.text).window.document;
		const composition: string = this.getProductCompositionListFromHtmlDocument(document);

		if (composition === '') return undefined;
		else return DbHandler.buildCompositionFromIngredientsList(composition);
	}

	private parseProductPageUrl(productPageUrl: string): AuchanGroupProductPageUrlElements {

		const match = productPageUrl.match(this.getProductPageUrlParser());

		if (!match || typeof match[0] === undefined || typeof match[0] === undefined) {
			throw new Error(`unexpected url architecture: ${productPageUrl}`);
		}

		return {
			id: match[2],
			name: match[1],
		};
	}
}
