// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import AuchanGroupDbHandler from './AuchanGroupDbHandler';

export default class AuchanDbHandler extends AuchanGroupDbHandler {

	private static productPageUrlParser: RegExp = /https:\/\/www\.auchandrive\.fr\/catalog\/(.+)-([A-Z\d]+)/;
	private static nutritionnalInfosFilter: RegExp = /^Autres\s+infos\s+nutritionnelles:.*?(?:Ingrédients:(.*))?$/;

	protected getProductPageUrlParser() {
		return AuchanDbHandler.productPageUrlParser;
	}

	protected buildProductPageUrl(productId: number, productName: string): string {
		return `https://www.auchandrive.fr/catalog/${productName}-${productId}`;
	}

	protected buildCookie(shopId: number): string {
		return `auchanCook="${shopId}|"`;
	}

	protected getProductCompositionListFromHtmlDocument(document: Document): string {

		let composition: string = '';

		const infoContainers = document.getElementsByClassName('pdp-bottom-infos__container');
		let title: any;
		let content: any;

		for (const infoContainer of infoContainers) {

			title = infoContainer.getElementsByTagName('span')[0];
			if (!title) continue;
			title = title.textContent;

			if (!title.match(/composition/i)) continue;

			content = infoContainer.getElementsByClassName('pdp-bottom-infos__content')[0];
			if (!content) break;
			content = content.textContent;

			const match: RegExpMatchArray = content.match(AuchanDbHandler.nutritionnalInfosFilter);

			if (match) composition += match[1] || '';
			else composition += content;

			break;
		}

		return composition;
	}
}
