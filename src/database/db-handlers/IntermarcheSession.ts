// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as request from 'superagent';
import { AnyObject } from '../../Types';
import { sendRequest } from '../../Utils';

interface Session {
	[key: string]: {
		expireDate: number,
		cookie: string,
	};
}

export default class IntermarcheSession {

	private static url = 'https://drive.intermarche.com/';
	private static fakeUserAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:64.0) Gecko/20100101 Firefox/64.0';
	private static sesssionIdPattern = /^ASP\.NET_SessionId=([a-z0-9]{24});/;
	private static sessionLifespan = 60000; // 1 minute in milliseconds

	private sessions: Session = {};

	public async getProductSheet(store: string, productID: number): Promise<AnyObject> {

		if (!this.sessions[store]) {
			await this.newSession(store);
		} else if (this.sessions[store].expireDate < Date.now()) {
			try {
				await this.newSession(store);
			} catch (err) {
				// never mind, a session exists, we can try to use it
			}
		}

		const session = this.sessions[store];

		const payload = JSON.stringify({
			idPdv: 'null',
			idProduit: '' + productID,
			trackingCode: '',
		});

		const req = request
			.post(IntermarcheSession.url + 'FicheProduit')
			.set('Cookie', session.cookie)
			.set('User-Agent', IntermarcheSession.fakeUserAgent)
			.set('Content-Type', 'application/json; charset=utf-8')
			.send(payload);

		const res = await sendRequest(req);
		return JSON.parse(res.text);
	}

	private async newSession(store: string): Promise<void> {

		const req = request
			.get(IntermarcheSession.url + store)
			.set('Cookie', 'sso_off=1')
			.set('User-Agent', IntermarcheSession.fakeUserAgent);

		const res = await sendRequest(req);

		if (!res.header['set-cookie']) {
			throw new Error('no Cookie header is set');
		}

		let match: RegExpMatchArray;
		let sessionId: string;

		for (const cookie of res.header['set-cookie']) {

			match = cookie.match(IntermarcheSession.sesssionIdPattern);

			if (match) {
				sessionId = match[1];
				break;
			}
		}

		if (!sessionId) throw new Error('the ASP.NET_SessionId cookie wasn\'t found');

		this.sessions[store] = {
			cookie: `ASP.NET_SessionId=${sessionId}; sso_off=1`,
			expireDate: Date.now() + IntermarcheSession.sessionLifespan,
		};
	}
}
