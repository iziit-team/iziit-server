// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as request from 'superagent';
import { AnyObject, Composition, Supplier } from '../../Types';
import { sendRequest } from '../../Utils';
import { MongodbProductDocument, MongodbShopDocument } from '../Documents';
import DbHandler from './DbHandler';

interface Shop {
	_id: number;
	name: string;
	loadBalancer: number;
}

interface Product {
	_id: number;
	name: string;
}

interface ParsedLeclercUrl {
	shop: Shop;
	product: Product;
}

export default class LeclercDbHandler extends DbHandler {

	private static urlParser: RegExp = /^https:\/\/fd(\d+)-courses\.leclercdrive\.fr\/magasin-(\d{6})-(.+?)\/fiche-produits-(\d+)-(.+)\.aspx$/;

	private static parseProductPageUrl(productPageUrl: string): ParsedLeclercUrl {

		const match = productPageUrl.match(LeclercDbHandler.urlParser);
		if (!match) throw new Error(`unexpected url architecture: ${productPageUrl}`);

		return {
			product: {
				_id: parseInt(match[4], 10),
				name: match[5],
			},
			shop: {
				_id: parseInt(match[2], 10),
				loadBalancer: parseInt(match[1], 10),
				name: match[3],
			},
		};
	}

	private shopsDbBuffer: {[key: number]: Shop} = {};
	private emptyBufferPromise: Promise<void>;

	public async init(): Promise<void> {
		setInterval(() => this.emptyBuffer(), 60000);
	}

	public async getProductComposition(productPageUrl: string): Promise<Composition> {

		const infos: ParsedLeclercUrl = LeclercDbHandler.parseProductPageUrl(productPageUrl);
		const document: MongodbProductDocument = await this.findDocument('shops', {_id: infos.shop._id}) as MongodbProductDocument;

		if (!document && !this.shopsDbBuffer[infos.shop._id]) {
			if (this.emptyBufferPromise) await this.emptyBufferPromise;
			this.shopsDbBuffer[infos.shop._id] = infos.shop;
		}

		return await super.getProductComposition2(infos.shop._id, infos.product._id, infos.product.name, () => this.getProductCompositionOnline(productPageUrl));
	}

	protected async buildOnlineProductCompositionSupplier(shopId: number, document: MongodbProductDocument): Promise<Supplier<Promise<Composition>>> {
		const productPageUrl: string = await this.buildProductPageUrl(shopId, document);
		return () => this.getProductCompositionOnline(productPageUrl);
	}

	private async getProductCompositionOnline(productPageUrl: string): Promise<Composition> {

		let content: string;

		try {
			content = (await sendRequest(request(productPageUrl))).text;
		} catch (err) {
			console.warn(`${err.status || err.code}: ${productPageUrl}`);
			return undefined;
		}

		const pageParser: RegExp = /Utilitaires\s*\.\s*widget\s*\.\s*initOptions\s*\(\s*'\w*'\s*,\s*((?:(?:(?:(?:\\"|[^"])*(?:[^\\"]|\\"))?"){2})*?(?:\\"|[^"])*)\s*\)\s*;/g;
		let match: RegExpMatchArray;
		let json: AnyObject;

		for (let i = 0; i < 100; ++i) {

			if (i === 99) {
				throw new Error(`max regex iterations reached for URL ${productPageUrl}, strange behaviour`);
			}

			match = pageParser.exec(content);
			if (!match) break;

			try {
				json = JSON.parse(match[1]);
			} catch (err) {
				console.error(`JSON parsing error with URL ${productPageUrl}: ${err}`);
				continue;
			}

			if (!json.objProduit || !json.objProduit.sComposition) continue;

			let composition: string = json.objProduit.sComposition;

			if (json.objProduit.sAllergenes) {
				composition += json.objProduit.sAllergenes + ' ' + composition;
			}

			composition = composition.replace(/<.*?>/g, '');
			return DbHandler.buildCompositionFromIngredientsList(composition);
		}
	}

	private async buildProductPageUrl(shopId: number, productDocument: MongodbProductDocument): Promise<string> {
		const shopDocument: MongodbShopDocument = await this.findDocument('shops', {_id: shopId}) as MongodbShopDocument;
		let formatedShopId: string = shopId + '';
		formatedShopId = '000000'.substr(0, 6 - formatedShopId.length) + shopId;
		return `https://fd${shopDocument.loadBalancer}-courses.leclercdrive.fr/magasin-${formatedShopId}-${shopDocument.name}/fiche-produits-${productDocument._id}-${productDocument.name}.aspx`;
	}

	private emptyBuffer(): void {

		if (Object.keys(this.shopsDbBuffer).length === 0) return;

		this.emptyBufferPromise = (async () => {
			const promises = [];

			for (const shopId in this.shopsDbBuffer) {
				promises.push(
					this.database
					.collection('shops')
					.insertOne(this.shopsDbBuffer[shopId])
					.catch(console.error));
			}

			await Promise.all(promises);
			this.shopsDbBuffer = {};
		})();
	}
}
