// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Composition, Supplier } from '../../Types';
import { mergeObjects } from '../../Utils';
import { MongodbProductDocument } from '../Documents';
import DbHandler from './DbHandler';
import IntermarcheSession from './IntermarcheSession';

export default class IntermarcheDbHandler extends DbHandler {

	private nbOfShops: number;
	private shopsDbBuffer: {[key: string]: number} = {};
	private emptyBufferPromise: Promise<void>;
	private session = new IntermarcheSession();

	public async init(): Promise<void> {
		this.nbOfShops = await this.database.collection('shops').countDocuments();
		setInterval(() => this.emptyBuffer(), 60000);
	}

	public async getProductComposition(shopName: string, productId: number): Promise<Composition> {
		const shopId = await this.getShopId(shopName);
		return await super.getProductComposition2(shopId, productId, undefined, () => this.getProductCompositionOnline(shopName, productId));
	}

	protected async buildOnlineProductCompositionSupplier(shopId: number, document: MongodbProductDocument): Promise<Supplier<Promise<Composition>>> {
		const shopName: string = await this.getShopName(shopId);
		return () => this.getProductCompositionOnline(shopName, document._id);
	}

	protected async getProductCompositionOnline(shopName: string, productId: number): Promise<Composition> {

		let productSheet: any;

		try {
			productSheet = await this.session.getProductSheet(shopName, productId);
		} catch (err) {
			console.warn(`${err.status}: shopName=${shopName}, productId=${productId}`);
			return undefined;
		}

		if (typeof productSheet.Fiche !== 'object'
				|| typeof productSheet.Fiche.Allergene !== 'string'
				|| typeof productSheet.Fiche.Ingredients !== 'string') {
			console.error(`unexpected productSheet architecture for product ${productId} at ${shopName}: ${JSON.stringify(productSheet)}`);
			return undefined;
		}

		const allergens = productSheet.Fiche.Allergene;
		const ingredients = productSheet.Fiche.Ingredients;

		return mergeObjects(
			DbHandler.buildCompositionFromIngredientsList(allergens),
			DbHandler.buildCompositionFromIngredientsList(ingredients),
			(a: string, b: string) => a || b,
		);
	}

	private async getShopName(shopId: number): Promise<string> {

		const document = await this.findDocument('shops', {_id: shopId}) as MongodbProductDocument;

		if (!document || !document.name) {
			throw new Error(`unknow shop: ${shopId}`);
		}

		return document.name;
	}

	private async getShopId(shopName: string): Promise<number> {

		const document = await this.findDocument('shops', {name: shopName});
		if (document) return document._id;

		if (this.emptyBufferPromise) await this.emptyBufferPromise;
		if (this.shopsDbBuffer[shopName] !== undefined) return this.shopsDbBuffer[shopName];

		this.shopsDbBuffer[shopName] = this.nbOfShops;
		return this.nbOfShops++;
	}

	private async emptyBuffer(): Promise<void> {

		if (Object.keys(this.shopsDbBuffer).length === 0) return;

		this.emptyBufferPromise = (async () => {
			const promises = [];

			for (const shopName in this.shopsDbBuffer) {
				promises.push(
					this.database
					.collection('shops')
					.insertOne({_id: this.shopsDbBuffer[shopName], name: shopName})
					.catch(console.error));
			}

			await Promise.all(promises);
			this.shopsDbBuffer = {};
		})();
	}
}
