// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Db, UpdateWriteOpResult } from 'mongodb';
import { supervisedIngredientsList, supervisedIngredientsRegex } from '../../SupervisedIngredients';
import { Composition, Consumer, Ingredient, Supplier } from '../../Types';
import { now, parallelExecution } from '../../Utils';
import { CompositionForEachShop, MongodbDocument, MongodbProductDocument } from '../Documents';

interface PtrQueueData {
	days: number;
	nbMaxOfProducts: number;
	resolve: Consumer<MongodbProductDocument[]>;
	reject: Consumer<any>;
}

export default abstract class DbHandler {

	protected static buildCompositionFromIngredientsList(ingredientsList: string): Composition {
		let regex: RegExp;
		const composition = this.newComposition();
		let ingredient: Ingredient;

		mainLoop: for (ingredient in supervisedIngredientsRegex) {
			if (Array.isArray(supervisedIngredientsRegex[ingredient])) {
				for (regex of supervisedIngredientsRegex[ingredient] as RegExp[]) {
					if (ingredientsList.match(regex)) {
						composition[ingredient] = true;
						continue mainLoop;
					}
				}
				composition[ingredient] = false;
			} else {
				regex = supervisedIngredientsRegex[ingredient] as RegExp;
				composition[ingredient] = ingredientsList.match(regex) ? true : false;
			}
		}

		return composition;
	}

	private static packComposition(composition: Composition): number {

		if (!composition) return -1;
		let packedComposition: number = 0;

		for (let i = 0; i < supervisedIngredientsList.length; ++i) {
			if (composition[supervisedIngredientsList[i]]) {
				packedComposition |= 1 << i;
			}
		}

		return packedComposition;
	}

	private static unpackComposition(packedComposition: number): Composition {
		if (packedComposition === -1) return undefined;
		const composition = this.newComposition();

		for (let i = 0; i < supervisedIngredientsList.length; ++i) {
			if (!(packedComposition >> i & 1)) {
				composition[supervisedIngredientsList[i]] = false;
			}
		}

		return composition;
	}

	private static newComposition(): Composition {
		// by default all ingredients are present (false positive is less dangerous than false negative)
		return {
			'celery': true,
			'crustaceans': true,
			'eggs': true,
			'fishes': true,
			'gluten': true,
			'lupine': true,
			'milk': true,
			'moluscs': true,
			'mustard': true,
			'nuts': true,
			'palm-oil': true,
			'peanut': true,
			'sesame-seeds': true,
			'soy': true,
			'sulfure-dioxyde-and-sulphites': true,
		};
	}

	protected database: Db;
	private ptrQueue: PtrQueueData[] = []; // here "ptr" means "product to refresh"
	private ptrQueueBusy: boolean = false;

	public constructor(database: Db) {
		this.database = database;
	}

	public async abstract init(): Promise<void>;

	public async deleteOldProducts(days: number): Promise<number> {
		const limit: number = now() - 24 * (days - 1);
		return (await this.database.collection('products').deleteMany({lastView: {$lt: limit}})).deletedCount;
	}

	public async getRefreshFunctionsOfOldProducts(days: number, nbMaxOfProducts: number): Promise<Array<Supplier<Promise<string>>>> {

		let docs: MongodbProductDocument[];

		try {
			docs = await this.getProductsToRefresh(days, nbMaxOfProducts);
		} catch (err) {
			console.error(err);
		}

		if (!docs || docs.length === 0) return [];

		// retrieves in an array, the refresh functions of all products
		return (await Promise.all(docs.map((doc) => this.getRefreshFunctionsOfAProduct(doc)))).reduce((accumulator, value) => accumulator.concat(value));
	}

	protected async updateDocument(collection: string, document: MongodbDocument, throwIfError: boolean): Promise<void> {
		try {
			await this.database.collection(collection).replaceOne({_id: document._id}, document, {upsert: true});
		} catch (err) {
			if (throwIfError) throw err;
			else console.error(err);
		}
	}

	protected findDocuments(collection: string, query: object, limit: number): Promise<MongodbDocument[]> {
		return new Promise((resolve, reject) => {
			this.database.collection(collection).find(query).limit(limit).toArray((err, docs: MongodbProductDocument[]) => {
				if (err) reject(err);
				else resolve(docs);
			});
		});
	}

	protected findDocument(collection: string, query: object): Promise<MongodbDocument> {
		return this.database.collection(collection).findOne(query);
	}

	protected async abstract buildOnlineProductCompositionSupplier(shopId: number, document: MongodbProductDocument): Promise<Supplier<Promise<Composition>>>;

	protected async getProductComposition2(shopId: number, productId: any, productName: string, onlineProductCompositionSupplier: Supplier<Promise<Composition>>): Promise<Composition> {

		let document: MongodbProductDocument = await this.findDocument('products', {_id: productId}) as MongodbProductDocument;
		const currentTimestamp: number = now();

		if (document) {

			let packedComposition: number;

			for (const c in document.composition) {
				if (~document.composition[c].indexOf(shopId)) {
					packedComposition = parseInt(c, 10);
					break;
				}
			}

			if (packedComposition !== undefined) {
				if (document.lastView !== currentTimestamp) {
					parallelExecution(() => {
						document.lastView = currentTimestamp;
						this.updateDocument('products', document, false);
					});
				}
				return DbHandler.unpackComposition(packedComposition);
			}
		}

		let documentModified: boolean = false;

		if (document) {
			if (document.lastView !== currentTimestamp) {
				document.lastView = currentTimestamp;
				documentModified = true;
			}
		} else {
			document = {
				_id: productId,
				composition: {},
				lastUpdate: currentTimestamp,
				lastView: currentTimestamp,
				name: productName,
			} as MongodbProductDocument;
			documentModified = true;
		}

		const composition: Composition = await onlineProductCompositionSupplier();

		parallelExecution(() => {
			const packedComposition: number = DbHandler.packComposition(composition);
			let found: boolean = false;

			for (const c in document.composition) {
				if (parseInt(c, 10) === packedComposition) {
					document.composition[c].push(shopId);
					found = true;
					documentModified = true;
					break;
				}
			}

			if (!found) {
				document.composition[packedComposition] = [shopId];
				documentModified = true;
			}

			if (documentModified) {
				if (document.name === undefined) delete document.name;
				this.updateDocument('products', document, false);
			}
		});

		return composition;
	}

	private getProductsToRefresh(days: number, nbMaxOfProducts: number): Promise<MongodbProductDocument[]> {

		const promise: Promise<MongodbProductDocument[]> = new Promise((resolve, reject) => {
			// Let push some data in the queue...
			this.ptrQueue.push({
				days,
				nbMaxOfProducts,
				reject,
				resolve, // ...with a callback...
			});
		});

		// ...then process the data and call the "resolve" callback when terminated.
		this.processPtrQueue();
		return promise;
	}

	private async processPtrQueue(): Promise<void> {

		if (this.ptrQueueBusy) return;
		this.ptrQueueBusy = true;

		let params: PtrQueueData;
		let currentTimestamp: number;
		let limit: number;
		let docs: MongodbProductDocument[];

		while (this.ptrQueue.length > 0) {

			params = this.ptrQueue.shift();
			currentTimestamp = now();
			limit = currentTimestamp - 24 * (params.days - 1);

			try {
				docs = await this.findDocuments('products', {lastUpdate: {$lt: limit}}, params.nbMaxOfProducts) as MongodbProductDocument[];
			} catch (err) {
				params.reject(err);
				continue;
			}

			const promises: Array<Promise<UpdateWriteOpResult>> = [];
			for (const doc of docs) {
				doc.lastUpdate = currentTimestamp;
				promises.push(this.database.collection('products').updateOne({_id: doc._id}, {$set: {lastUpdate: currentTimestamp}}));
			}

			try {
				await Promise.all(promises);
			} catch (err) {
				params.reject(err);
				continue;
			}

			params.resolve(docs);
		}

		this.ptrQueueBusy = false;
	}

	private async getRefreshFunctionsOfAProduct(document: MongodbProductDocument): Promise<Array<Supplier<Promise<string>>>> {

		const funcs: Array<Supplier<Promise<string>>> = [];
		let nbOfResolvedFuncs: number = 0;
		const newComposition: CompositionForEachShop = {};

		let modified: boolean = false;

		for (const composition in document.composition) {
			for (const shopId of document.composition[composition]) {

				const getProductCompositionOnline: Supplier<Promise<Composition>> = await this.buildOnlineProductCompositionSupplier(shopId, document);

				funcs.push(async () => {

					let upToDateComposition: number;
					let status: string;

					try {
						upToDateComposition = DbHandler.packComposition(await getProductCompositionOnline());

						if (upToDateComposition === parseInt(composition, 10)) {
							status = 'preserved';
						} else {
							if (upToDateComposition === -1) {
								status = 'removed';
							} else {
								if (newComposition[upToDateComposition]) {
									newComposition[upToDateComposition].push(shopId);
								} else {
									newComposition[upToDateComposition] = [shopId];
								}
								status = 'updated';
							}
							modified = true;
						}

					} catch (err) {
						console.error(err);
						modified = true;
						status = 'removed';
					}

					nbOfResolvedFuncs++;

					if (nbOfResolvedFuncs === funcs.length && modified) {
						if (Object.keys(newComposition).length === 0) {
							this.database
								.collection('products')
								.deleteOne({_id: document._id})
								.catch(console.warn);
						} else {
							this.database
								.collection('products')
								.updateOne({_id: document._id}, {$set: {composition: newComposition}})
								.catch(console.warn);
						}
					}

					return status;
				});
			}
		}

		return funcs;
	}
}
