// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { MongoClient } from 'mongodb';
import { addonBackendConfig } from '../Configuration';
import { AnyObject, Brand, Supplier } from '../Types';
import BufferExecutor from './BufferExecutor';
import AuchanDbHandler from './db-handlers/AuchanDbHandler';
import ChronodriveDbHandler from './db-handlers/ChronodriveDbHandler';
import DbHandler from './db-handlers/DbHandler';
import IntermarcheDbHandler from './db-handlers/IntermarcheDbHandler';
import LeclercDbhandler from './db-handlers/LeclercDbHandler';

type DbHandlers = {
	[key in Brand]: DbHandler
};

export default class DbHandlersCoordinator {

	private nbOfDaysBeforeDeletionWhenUnused: number;
	private nbOfDaysBeforeUpdating: number;
	private connectionString: string;
	private databaseOptions: AnyObject;
	private dbHandlers: DbHandlers;

	public constructor() {

		const config = addonBackendConfig.databases;

		this.nbOfDaysBeforeDeletionWhenUnused = config.nbOfDaysBeforeDeletionWhenUnused;
		this.nbOfDaysBeforeUpdating = config.nbOfDaysBeforeUpdating;
		this.databaseOptions = config.options || {};
		this.scheduleNextUpdate(config.updateTime);

		const host = config.host ?? 'localhost';
		const port = config.port ?? 27017;

		if (config.credentials) {
			const login = encodeURIComponent(config.credentials.login);
			const password = encodeURIComponent(config.credentials.password);
			const authSource = encodeURIComponent(config.credentials.authSource);
			this.connectionString = `mongodb://${login}:${password}@${host}:${port}/?authSource=${authSource}`;
		} else {
			this.connectionString = `mongodb://${host}:${port}`;
		}
	}

	public async init(): Promise<void> {

		const mongoClient = await MongoClient.connect(this.connectionString, this.databaseOptions);
		this.dbHandlers = {
			auchan: new AuchanDbHandler(mongoClient.db('auchan')),
			chronodrive: new ChronodriveDbHandler(mongoClient.db('chronodrive')),
			intermarche: new IntermarcheDbHandler(mongoClient.db('intermarche')),
			leclerc: new LeclercDbhandler(mongoClient.db('leclerc')),
		};

		const initPromises = [];
		for (const brand in this.dbHandlers) initPromises.push(this.dbHandlers[brand as Brand].init());
		await Promise.all(initPromises);
	}

	public getDbHandler(brand: Brand): DbHandler {
		return this.dbHandlers[brand];
	}

	public scheduleNextUpdate(timestampOrTime: number|string): void {

		let nextUpdateTimestamp: number;

		if (typeof timestampOrTime === 'string') {

			const parsedTime: number[] = timestampOrTime.split(':').map((x) => parseInt(x, 10));

			const nextUpdateDate: Date = new Date();
			nextUpdateDate.setHours(parsedTime[0]);
			nextUpdateDate.setMinutes(parsedTime[1]);
			nextUpdateDate.setSeconds(0);
			nextUpdateDate.setMilliseconds(0);

			nextUpdateTimestamp = nextUpdateDate.getTime();
			if (Date.now() > nextUpdateTimestamp) nextUpdateTimestamp += 86400000; // add a day

		} else {
			nextUpdateTimestamp = timestampOrTime;
		}

		setTimeout(async () => {
			try {
				await this.updateDatabases();
			} catch (err) {
				console.error(`database updating finished at ${new Date().toUTCString()} with an error: ${err}`);
			}

			while (Date.now() > nextUpdateTimestamp) nextUpdateTimestamp += 86400000; // add a day
			this.scheduleNextUpdate(nextUpdateTimestamp);

		}, nextUpdateTimestamp - Date.now());
	}

	private async updateDatabases(): Promise<void> {

		console.log('start database updating at ' + new Date().toUTCString());

		const promises: Array<Promise<number>> = [];

		for (const brand in this.dbHandlers) {
			const promise = this.dbHandlers[brand as Brand].deleteOldProducts(this.nbOfDaysBeforeDeletionWhenUnused);
			promise.then((deleteCount: number) => {
				console.log(`${deleteCount} products were deleted from ${brand} database.`);
			}).catch((err: string) => {
				console.error(`an error occured during products deletions from ${brand} database: ${err}`);
			});
			promises.push(promise);
		}

		await Promise.all(promises);

		console.log('old products deletion has terminated at ' + new Date().toUTCString());
		console.log('now checking products composition');

		let removed: number = 0;
		let updated: number = 0;
		let preserved: number = 0;
		let total: number = 0;

		const bufferExecutor: BufferExecutor = new BufferExecutor(100);

		bufferExecutor.setFillNeededListener(async () => {

			let refreshFuncs: Array<Supplier<Promise<string>>> = [];

			for (const brand in this.dbHandlers) {
				refreshFuncs = refreshFuncs.concat(await this.dbHandlers[brand as Brand].getRefreshFunctionsOfOldProducts(this.nbOfDaysBeforeUpdating, 100));
			}

			const wrappedRefreshFuncs: Array<Supplier<Promise<void>>> = refreshFuncs.map((refreshFunc) => async () => {
				switch (await refreshFunc()) {
					case 'preserved': preserved++; break;
					case 'updated':  updated++; break;
					case 'removed': removed++; break;
				}
				total++;
			});

			if (wrappedRefreshFuncs.length === 0) bufferExecutor.terminate();
			else bufferExecutor.add(wrappedRefreshFuncs);
		});

		bufferExecutor.setTerminatedListener(() => {
			const toPercentage = (x: number) => Math.round(x / total * 10000) / 100 + '%';

			console.log('products composition checking ended at ' + new Date().toUTCString());
			if (total === 0) console.log('0 product checked.');
			else console.log(`${total} products checked (${toPercentage(preserved)} preserved, ${toPercentage(updated)} updated and ${toPercentage(removed)} removed).`);
			console.log('end database updating at ' + new Date().toUTCString());
		});

		bufferExecutor.start();
	}
}
