// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Runnable } from '../Types';

export default class BufferExecutor {

	private size: number;
	private fillNeededListener: Runnable;
	private terminatedListener: Runnable;
	private scheduled: Runnable[] = [];
	private executing: Runnable[] = [];
	private terminated: boolean = false;
	private terminatedListenerTriggered: boolean = false;

	public constructor(size: number) {
		this.size = size;
	}

	public setTerminatedListener(listener: Runnable): void {
		this.terminatedListener = listener;
	}

	public setFillNeededListener(listener: Runnable): void {
		this.fillNeededListener = listener;
	}

	public start(): void {
		if (this.size > 0) this.fillNeededListener();
	}

	public add(funcs: Runnable[]): void {
		this.scheduled = this.scheduled.concat(funcs);
		this.check();
	}

	public terminate() {
		this.terminated = true;
		this.triggerTerminatedListener();
	}

	private check() {

		while (this.executing.length < this.size && this.scheduled.length > 0) {

			const toExec: Runnable = this.scheduled.shift();
			this.executing.push(toExec);

			(async () => {
				await toExec();
				this.executing.splice(this.executing.indexOf(toExec), 1);
				this.check();
			})();
		}

		if (this.terminated) {
			this.triggerTerminatedListener();
		} else if (this.executing.length < this.size && this.fillNeededListener !== undefined) {
			this.fillNeededListener();
		}
	}

	private triggerTerminatedListener() {
		if (this.terminated
			&& !this.terminatedListenerTriggered
			&& this.scheduled.length === 0
			&& this.executing.length === 0
			&& typeof this.terminatedListener === 'function') {
				this.terminatedListenerTriggered = true;
				this.terminatedListener();
		}
	}
}
