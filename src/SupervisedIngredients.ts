// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Ingredient,  IngredientsList } from './Types';

export const supervisedIngredientsList: IngredientsList = [
	'palm-oil',
	'gluten',
	'crustaceans',
	'eggs',
	'fishes',
	'peanut',
	'soy',
	'milk',
	'nuts',
	'celery',
	'mustard',
	'sesame-seeds',
	'sulfure-dioxyde-and-sulphites',
	'lupine',
	'moluscs',
];

export const supervisedIngredientsRegex: {[k in Ingredient]: RegExp | RegExp[]} = {
	'celery': /(^|\W)(?<!sans\s+)c[ée\?]leris?($|\W)/i,
	'crustaceans': /(^|\W)((?<!sans\s+)crustac[ée\?]s?|(araign[ée\?]e|cigale|escargot)s?\s+des?\s+mers?|crabes?|crevettes?|[ée\?]crevisses?|[ée\?]trilles?|gambas|homards?|langoustes?|squilles?|tourteaux?)($|\W)/i,
	'eggs': /(^|\W)(?<!sans\s+)(œ|oe|\?)ufs?($|\W)/i,
	'fishes': [
		/(^|\W)(?<!sans\s+)poissons?($|\W)/i,
		/(^|\W)((ai|[ée\?])glefin|aiguillat|anchoi|anguille|[âa\?]non)s?($|\W)/i,
		/(^|\W)(bar|barbue|baudroie|blanquette|br[èe\?]me|brosome)s?($|\W)/i,
		/(^|\W)(cabillauds?|canth[èe\?]res?|capelans?|carrelets?|[cs][ée\?]teaux?|(chat|chien|crapaud)s?\s+de\s+mer|colin(et)?s?|congres?|courbines?)($|\W)/i,
		/(^|\W)(diables?\s+de\s+mer|d(au|o)rades?)($|\W)/i,
		/(^|\W)(empereur|[ée\?]perlan|espadon|esprot)s?($|\W)/i,
		/(^|\W)(fl[ée\?]tan|fugu)s?($|\W)/i,
		/(^|\W)(girelle|goberge|grenadier|griset|grogneur|grondin)s?($|\W)/i,
		/(^|\W)(haddock|hareng|hoplost[èe\?]the)s?($|\W)/i,
		/(^|\W)juliennes?($|\W)/i,
		/(^|\W)(lamproies?|langues?\s+d\s*'\s*avocats?|lieux?\s+(noirs?|jaunes?|de\s+l\s*'\s*alaska|de\s+norv[ée\?]ge)|limandes?|lingues?|loquettes?|lottes?|loubines?|loups?\s+de\s+(l\s*'\s*atlantique|mer))($|\W)/i,
		/(^|\W)(maigres?|maquereaux?|mara[îi\?]ches?|menuises?|merlans?|merlu(che)?s?|m[ée\?]rous?|molvas?|morues?|mostelles?|muges?|mulets?\s+gris)($|\W)/i,
		/(^|\W)(ogac|orphie)s?($|\W)/i,
		/(^|\W)(pageots?|pagres?|p[ée\?]lamides?|plies?|pocheteaux?)($|\W)/i,
		/(^|\W)(raie|rascasse|rouget|roussette|requin)s?($|\W)/i,
		/(^|\W)(sabre|sardine|saumon(ette)?|s[ée\?]baste|serran|sole|sprot|sprat)s?($|\W)/i,
		/(^|\W)(tanche|tanude|thazard|thon|truite|turbot)s?($|\W)/i,
		/(^|\W)(veaux?\s+de\s+mer|vivaneaux?|vives?)($|\W)/i,
	],
	'gluten': /(^|\W)(?<!sans\s+)(gluten|bl[ée\?]|seigle|orge|avoine|[ée\?]peautre|kamut)($|\W)/i,
	'lupine': /(^|\W)(?<!sans\s+)lupins?($|\W)/i,
	'milk': /(^|\W)(?<!sans\s+)(lait(iers?)?|lactose)($|\W)/i,
	'moluscs': [
		/(^|\W)(?<!sans\s+)mollusques?($|\W)/i,
		/(^|\W)(acm[ée\?]e|anomie|arche|avicule)s?|amandes?\s+d[eu]\s+(l'atlantique|m[ée\?]diterran[ée\?]e|mer|pacifique)|arap[èe\?]des?\s+du\s+pacifique($|\W)/i,
		/(^|\W)(bernique|bucarde|buccin|bul(ot|llie)|busycon)s?|berlingots?\s+de\s+mer|bigorneaux?($|\W)/i,
		/(^|\W)(cardite|casque|chanque|clam|clausinelle|palourde|clovisse|(?<!fruits?\s+[àa\?]\s+)coque|corbicule|couteau|cr[ée\?]pidule|cyprine|cyr[èe\?]ne|cyth[ée\?]r[ée\?]e)s?($|\W)/i,
		/(^|\W)darines?|dattes?\s+de?\s+mer|donaces?|dosinies?($|\W)/i,
		/(^|\W)[ée\?]g[ée\?]ries?($|\W)/i,
		/(^|\W)(fasciolaire|fissurelle|flion)s?($|\W)/i,
		/(^|\W)(gallinette|gofiche|goufique)s?($|\W)/i,
		/(^|\W)haricots?\s+de\s+mer|hu[îi\?]tres?($|\W)/i,
		/(^|\W)isocardes?($|\W)/i,
		/(^|\W)jambonneaux?\s+de\s+mer($|\W)/i,
		/(^|\W)(lambi|lavignon|littorine|lucine|lutraire)s?($|\W)/i,
		/(^|\W)(m[ée\?]long[èe\?]ne|m[ée\?]sodesme|macome|mactre|modiole|montre|moule|mye)s?|marteaux?|murex($|\W)/i,
		/(^|\W)(n[ée\?]rite|nasse|natice)s?($|\W)/i,
		/(^|\W)olives?\s+de\s+mer|ormeaux?|ovarques?($|\W)/i,
		/(^|\W)(p[ée\?]tricole|palourde|panop[ée\?]e|patelle|pholade|pintadine|pitar|pourpre|praire|psammobie|pt[ée\?]roc[èe\?]re|pycnodonte)s?($|\W)/i,
		/(^|\W)rangie|rocher($|\W)/i,
		/(^|\W)(s[ée\?]mélé|saxidome|silique|sol[ée\?]curte|spisule|spondyle|strombe)s?|sanguines?\s+de\s+mer($|\\W)/i,
		/(^|\W)(tagal|telline|tivel|tonnelet|toutout|tritonous?\s+conque|trophe|troque|turban)s?($|\W)/i,
		/(^|\W)v[ée\?]nus|vernis|volute(ou)?s?($|\W)/i,
		/(^|\W)yets?($|\W)/i,
	],
	'mustard': /(^|\W)(?<!sans\s+)moutardes?($|\W)/i,
	'nuts': /(^|\W)(?<!sans\s+)(fruits?\s+[àa\?]\s+coques?|amandes?|noisettes?|pistaches?|noix(\s+d[eu]\s+(cajou|p[ée\?]can|macadamia|br[ée\?]sil|queensland))?)($|\W)/i,
	'palm-oil': /(^|\W)(?<!sans\s+huile\s+de\s+)palme($|\W)/i,
	'peanut': /(^|\W)(?<!sans\s+)(arachides?|cacaho?u[èe\?]tt?es?|pois\s+de\s+terre|pistaches?\s+de\s+terre|pinottes?)($|\W)/i,
	'sesame-seeds': /(^|\W)(?<!sans\s+)graines?\s+de\s+s[ée\?]same($|\W)/i,
	'soy': /(^|\W)(?<!sans\s+)soja($|\W)/i,
	'sulfure-dioxyde-and-sulphites': /(^|\W)(?<!sans\s+)(s\s*o\s*2|dioxydes?\s+de\s+soufre|anhydrides?\s+sulfureux|sulfites?)($|\W)/i,
};

export function isIngredientList(x: any): x is IngredientsList {
	if (!Array.isArray(x)) return false;
	const ingredients = supervisedIngredientsList.slice(0);
	if (x.length === 0 || x.length > ingredients.length) return false;
	let index;
	for (const element of x) {
		index = ingredients.indexOf(element);
		if (~index) ingredients.splice(index, 1);
		else return false;
	}
	return true;
}
