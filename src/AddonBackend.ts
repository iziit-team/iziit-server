// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import * as escapeStringRegexp from 'escape-string-regexp';
import { NextFunction, Request, Response } from 'express';
import Api from './api/Api';
import AuchanApi from './api/AuchanApi';
import ChronodriveApi from './api/ChronodriveApi';
import IntermarcheApi from './api/IntermarcheApi';
import LeclercApi from './api/LeclercApi';
import DbHandler from './database/db-handlers/DbHandler';
import DbHandlersCoordinator from './database/DbHandlersCoordinator';
import { Brand, PayloadBase } from './Types';

type Apis = {
	[key in Brand]: Api<DbHandler, PayloadBase>
};

export default class AddonBackend {

	public static dbHandlersCoordinator: DbHandlersCoordinator;

	private static apis: Apis;
	private static urlPattern = new RegExp(`^/api/(${Object.values(Brand).map(escapeStringRegexp).join('|')})/check/?$`);

	public static async init() {
		AddonBackend.dbHandlersCoordinator = new DbHandlersCoordinator();
		await AddonBackend.dbHandlersCoordinator.init();
		AddonBackend.apis = {
			auchan: new AuchanApi(),
			chronodrive: new ChronodriveApi(),
			intermarche: new IntermarcheApi(),
			leclerc: new LeclercApi(),
		};
	}

	public static middleware(req: Request, res: Response, next: NextFunction) {
		if (req.method === 'POST' && req.url.match(AddonBackend.urlPattern)) {
			AddonBackend.apis[RegExp.$1 as Brand].handleClientRequest(req, res);
		} else {
			next();
		}
	}
}
