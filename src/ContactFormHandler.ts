// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Request, Response } from 'express';
import Skeletons from 'skeletons';
import { post } from 'superagent';
import { serverConfig } from './Configuration';
import { ReCaptchaApiResponse } from './Types';
import { sendRequest } from './Utils';

interface ContactPayload {
	captchaResponse: string;
	email: string;
	message: string;
}

const payloadValidator = new Skeletons({
	captchaResponse: Skeletons.String({
		match: /^[a-zA-Z0-9_-]+$/,
	}),
	email: Skeletons.String(),
	message: Skeletons.String({
		match: /^.*[^\s].*$/, // not an empty message
	}),
}, {
	console: false,
});

function isPayloadValid(payload: any): payload is ContactPayload {
	return payloadValidator.validate(payload).valid;
}

async function verifyCaptha(clientIp: string, captchaResponse: string): Promise<void> {

	const url = 'https://www.google.com/recaptcha/api/siteverify'
		+ '?secret=' + serverConfig.reCaptchaSecretKey
		+ '&response=' + captchaResponse
		+ '&remoteip=' + clientIp;

	let response: ReCaptchaApiResponse;

	try {
		const rawResponse = await sendRequest(post(url));
		response = JSON.parse(rawResponse.text);
	} catch {
		throw 500;
	}

	if (!response.success) {
		throw 400;
	}
}

export default async function contactFormHander(req: Request, res: Response) {

	let payload: ContactPayload;

	if (isPayloadValid(req.body)) {
		payload = req.body;
	} else {
		res.status(400).send();
		return;
	}

	try {
		await verifyCaptha(req.ip, payload.captchaResponse);
	} catch (statusCode) {
		res.status(statusCode).send();
		return;
	}

	const user = payload.email === '' ? 'anonymous' : `[${payload.email}]`;

	const from = 'noreply@iziit.org';
	const subject = `a user wrote us via the form as ${user}`;
	const to = ['contact@iziit.org'];
	const msg = payload.message;

	// TODO: send mail
	res.status(501).send();
}
