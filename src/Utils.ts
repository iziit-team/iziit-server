// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { accessSync, constants } from 'fs';
import { Response, SuperAgentRequest } from 'superagent';
import { AnyObject, BiFunction, Runnable } from './Types';

export function parallelExecution(f: Runnable): void {
	(async () => {
		await Promise.resolve();
		f();
	})();
}

export function sendRequest(req: SuperAgentRequest): Promise<Response> {
	return new Promise((resolve, reject) => {
		req.end((err: any, res: Response) => {
			if (err) reject(err);
			else resolve(res);
		});
	});
}

/**
 * @param {AnyObject} first first object to merge
 * @param {AnyObject} second second object to merge. It must have the same fields name that the first.
 * @param {function(a, b)} merger Takes as argument a field of the first object and a field of the second. Retrieves the desired result.
 * @returns {AnyObject} an object that contains all merger results
 */
export function mergeObjects<T extends AnyObject>(first: T, second: T, merger: BiFunction<any, any, any>): T {

	const merged: AnyObject = {};

	for (const fieldName in first) {
		merged[fieldName] = merger(first[fieldName], second[fieldName]);
	}

	return merged as T;
}

/**
 * @returns {number} the number of days passed from 1800
 */
export function now(): number {
	return Math.floor(Date.now() / 3600000);
}

export function isFileReadable(path: string): boolean {
	try {
		accessSync(path, constants.F_OK | constants.R_OK);
		return true;
	} catch {
		return false;
	}
}
