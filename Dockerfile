FROM node:13 as builder
RUN mkdir /build/
WORKDIR /build/
COPY src/ /build/src/
COPY www-src/ /build/www-src/
COPY package.json postcss.config.js tsconfig.json /build/
RUN npm i
RUN npm run build-back
RUN npm run build-front

FROM node:13-alpine
RUN mkdir /app
WORKDIR /app
COPY --from=builder /build/build /app/server/
COPY --from=builder /build/www /app/www/
COPY package.json /app/
ENV NODE_ENV production
RUN npm i --only=prod
EXPOSE 80
CMD node server/Main.js
